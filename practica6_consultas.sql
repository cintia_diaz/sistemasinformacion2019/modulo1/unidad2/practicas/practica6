USE practica6;


/*
  CONSULTA 1: 
*/
-- c1: Temas de Bases de datos
SELECT 
  * 
FROM tema t 
WHERE t.descripcion='Bases de datos';

-- c2: Art�culos escritos en 1990
SELECT 
  * 
FROM articulo a 
WHERE a.anio='1990';

-- final: Art�culos sobre Bases de Datos escritos en 1990
SELECT 
  c2.titulo_art 
FROM (SELECT * FROM tema t WHERE t.descripcion='Bases de datos') c1
JOIN (SELECT * FROM articulo a WHERE a.anio='1990') c2
USING (codtema);

/*
  CONSULTA 2: 
*/

  -- c1: N�mero de temas
  SELECT 
    COUNT(*) 
  FROM tema;

  -- c2: Referencias de revistas que tengan todos los temas
  SELECT
    referencia 
  FROM (SELECT DISTINCT a.referencia, a.codtema FROM articulo a) c1 
  GROUP BY referencia 
  HAVING COUNT(*)=(SELECT COUNT(*) FROM tema t);

  -- final: T�tulos de revistas que tengan todos los temas
  SELECT r.titulo_rev 
  FROM revista r
  JOIN (SELECT 
          referencia 
        FROM (SELECT DISTINCT a.referencia, a.codtema FROM articulo a) c1 
        GROUP BY referencia 
        HAVING COUNT(*)=(SELECT COUNT(*) FROM tema t)
    ) c2
  USING (referencia);

/*
  CONSULTA 3:
*/  
  -- c1: Referencias de revistas
  SELECT 
    r.referencia 
  FROM revista r;

  -- Temas que no sean Medicina
  SELECT 
    * 
  FROM tema t 
  WHERE t.descripcion<>'Medicina';


  -- c2: Referencias de revistas con art�culos que no sean de medicina
  SELECT
    DISTINCT a.referencia 
  FROM (SELECT * FROM tema t WHERE t.descripcion<>'Medicina') c1 
  JOIN articulo a 
  USING (codtema);

  -- Referencias de revistas que no tienen articulos de medicina
  SELECT 
    c1.referencia 
  FROM (SELECT r.referencia FROM revista r) c1 
  LEFT JOIN (SELECT DISTINCT a.referencia FROM (SELECT * FROM tema t WHERE t.descripcion<>'Medicina') c1 JOIN articulo a USING (codtema)) c2
  USING (referencia) 
  WHERE c2.referencia IS NULL;

  -- final: T�tulos de revistas que ninguno de sus art�culos sean de medicina
  SELECT r.titulo_rev
  FROM (
        SELECT c1.referencia FROM (SELECT r.referencia FROM revista r
          ) c1 
        LEFT JOIN (
          SELECT DISTINCT a.referencia FROM (SELECT * FROM tema t WHERE t.descripcion<>'Medicina') c1 JOIN articulo a USING (codtema)
          ) c2
        USING (referencia)
        WHERE c2.referencia IS NULL
    ) c3
  JOIN 
  revista r
  USING (referencia);


/*
  CONSULTA 4:
*/

  -- c1: Autores que han escrito art�culos en 1991 sobre SQL

  SELECT
    c1.dni 
  FROM (SELECT * FROM articulo a WHERE a.anio='1991') c1 
  JOIN (SELECT * FROM tema t WHERE t.descripcion='SQL') c2 
  USING (codtema);
  
  -- c2: Autores que han escrito art�culos en 1992 sobre SQL
  SELECT 
    c1.dni 
  FROM (SELECT * FROM articulo a WHERE a.anio='1992') c1 
  JOIN (SELECT * FROM tema t WHERE t.descripcion='SQL') c2 
  USING (codtema);

  -- final: Nombre de los autores que han escrito sobre SQL en 1991 y en 1992
  SELECT a.nombre FROM 
    (
    SELECT c1.dni FROM (SELECT * FROM articulo a WHERE a.anio='1991') c1 JOIN (SELECT * FROM tema t WHERE t.descripcion='SQL') c2 USING (codtema)
    ) c1
    JOIN
    (
    SELECT c1.dni FROM (SELECT * FROM articulo a WHERE a.anio='1992') c1 JOIN (SELECT * FROM tema t WHERE t.descripcion='SQL') c2 USING (codtema)
    ) c2
    USING (dni)
    JOIN autor a
    USING (dni);


  
/*
  CONSULTA 5:
*/

-- c1: Art�culos escritos en 1993
SELECT
  * 
FROM articulo a
WHERE a.anio='1993';


-- c2: Autores que pertenecen a la universidad Polit�cnica de Madrid
SELECT 
  * 
FROM autor a 
WHERE a.universidad='Politecnica de Madrid';


-- final: Art�culos escritos en 1993 por autores de la Polit�cnica de Madrid
SELECT 
  c1.titulo_art 
FROM (SELECT * FROM articulo a WHERE a.anio='1993') c1
JOIN (SELECT * FROM autor a WHERE a.universidad='Politecnica de Madrid') c2
USING (dni);
